import { AccountCash } from './account-cash';
import { AccountCashGridHeader } from './account-cash-grid-header';

export interface AccountCashJson {
    noOfRecords: Number;
    recordsOnInitialLoad: Number;
    gridHeaderColumns: AccountCashGridHeader[];
    accountCash: AccountCash[];
}
