export class AccountCash {
    account: String;
    availCash: Number;
    todayChangePerc: Number;
    todayChangePercStr: String;
    todayChange: Number;
}
