import { Injectable } from '@angular/core';
import { AccountCashJson } from './account-cash-json';
@Injectable()
export class AccountCashService {

  constructor() { }

  getAccountData() {
    const res = {
      noOfRecords: 6,
      recordsOnInitialLoad: 3,
      gridHeaderColumns:
      [
        {
          columnId: 'account',
          columnName: 'Account'
        },
        {
          columnId: 'cash-and-change',
          columnName: '<span>Available Cash</span><div class="color-grey">Today\'s Change</div>'
        }
      ],
      accountCash: [
        {
          account: 'IRA - 5200',
          availCash: 5763.36,
          todayChangePerc: -0.08,
          todayChangePercStr: '-0.08',
          todayChange: 8916.69
        },
        {
          account: 'AAA - 3810',
          availCash: 10050054.07,
          todayChangePerc: 0.07,
          todayChangePercStr: '+0.07',
          todayChange: 8916.69
        },
        {
          account: 'REG - 2019',
          availCash: 13465679.34,
          todayChangePerc: 0.0,
          todayChangePercStr: '0.00',
          todayChange: 0.00
        },
        {
          account: 'AAA - 1812',
          availCash: 2010926.10,
          todayChangePerc: 0.21,
          todayChangePercStr: '+0.21',
          todayChange: 38881.63
        },
        {
          account: 'IRA - 0146',
          availCash: 15884302.39,
          todayChangePerc: 0.03,
          todayChangePercStr: '+0.03',
          todayChange: 7430.83
        },
        {
          account: 'AAA - 0029',
          availCash: 39160334.42,
          todayChangePerc: -0.07,
          todayChangePercStr: '-0.07',
          todayChange: 31435.87
        }
      ]
    } as AccountCashJson;
    return res;
  }
}
