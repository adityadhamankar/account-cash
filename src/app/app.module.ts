import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AccountCashService } from './components/account-cash/account-cash.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [ AccountCashService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
