import { Component, OnInit } from '@angular/core';
import { AccountCashService } from './components/account-cash/account-cash.service';
import { AccountCash } from './components/account-cash/account-cash';
import { AccountCashJson } from './components/account-cash/account-cash-json';
import { AccountCashGridHeader } from './components/account-cash/account-cash-grid-header';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  // Grid data
  accountCashData = {} as AccountCashJson;

  // Load more/less setting
  loadAllValues: Boolean = false;
  moreRecordsText: String = 'Load more';

  // Sort setting
  sortedOn: String;
  sortedDirection: String;

  constructor(private accountCashService: AccountCashService) {
  }

  ngOnInit() {
    this.accountCashData = this.accountCashService.getAccountData();
    this.sortData('cash-and-change', 'asc');
  }

  // Load more/less function
  loadMoreOrLess() {
    this.loadAllValues = !this.loadAllValues;
    this.moreRecordsText = (this.loadAllValues) ? 'Load less' : 'Load more';
  }

  // Sort function on click of header
  sortColumn(column: AccountCashGridHeader) {
    let direction: String;
    if (column.columnId === this.sortedOn) {
      direction = (this.sortedDirection === 'asc') ? 'desc' : 'asc';
    } else {
      direction = 'asc';
    }
    this.sortData(column.columnId, direction);
  }

  private sortData(column, direction) {
    // console.log(column, direction);
    const param = (column === 'cash-and-change') ? 'availCash' : 'account';

    this.sortedOn = column;
    this.sortedDirection = direction;

    this.accountCashData.accountCash.sort((a: any, b: any) => {
      const aNumber = (typeof a[param] === 'number') ? a[param] : a[param].split('-')[1];
      const bNumber = (typeof b[param] === 'number') ? b[param] : b[param].split('-')[1];

      if (aNumber < bNumber) {
         if (direction === 'asc') {
           return -1;
         } else {
           return 1;
         }

      } else if (aNumber > bNumber) {
         if (direction === 'asc') {
           return 1;
         } else {
           return -1;
         }

      } else {
        return 0;
      }
    });
  }
}
